trigger Account_Trigger on Account (Before insert, Before update) {
    
    for (Integer i = 0; i < Trigger.new.size(); i++)
        {
         try
         {
         	Test_Time.testtime();
            if((Trigger.isInsert || Trigger.isUpdate) && (Trigger.new[i].Approval_Status__c == 'Reject' && Trigger.old[i].Approval_Status__c != 'Reject'))
            {
                 rejectRecord(Trigger.new[i]);
            }
         }catch(Exception e)
         {
             Trigger.new[i].addError(e.getMessage());
         }
        }
        
    public void rejectRecord(Account opp)
        {
            String opptyId = opp.id;
            if (opptyId != null) {
            Account Accnt = [Select Id, (Select TargetObjectId, SystemModstamp, StepStatus, RemindersSent, ProcessInstanceId, OriginalActorId, IsPending, IsDeleted, Id, CreatedDate, CreatedById, Comments, ActorId From ProcessSteps order by SystemModstamp desc) from Account where Id = :opptyId];
            
            for(Integer i=0; i<Accnt.ProcessSteps.size(); i++)
            {
                System.debug(Accnt.ProcessSteps[i]);
                if(Accnt.ProcessSteps[i].StepStatus == 'Rejected' && Accnt.ProcessSteps[i].Comments == null || Accnt.ProcessSteps[0].Comments == '')
                    Trigger.new[0].addError('You have rejected the request, please add comments in comments field');
                
                break;
            }            
            }
        }
}