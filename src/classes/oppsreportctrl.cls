public with sharing class oppsreportctrl {
    public List<Opportunity> getOpps(){
        return [SELECT Name, Owner.Name, Amount, StageName FROM Opportunity LIMIT 10];
    }
}