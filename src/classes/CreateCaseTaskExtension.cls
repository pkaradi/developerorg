public with sharing class CreateCaseTaskExtension {
    private Case c;
    public CreateCaseTaskExtension(ApexPages.StandardController theController) {
        this.c = (Case)theController.getRecord();
        defineNewTask();
    }
    public Task theTask {get; set;}
    public String lastError {get; set;}
    public void defineNewTask(){
        theTask = new Task();
        theTask.WhatId = c.id;
        theTask.WhoId = c.ContactId;
        theTask.Priority = 'Normal';
        lastError = '';
    }
    public PageReference createTask() {
        createNewTask();
        defineNewTask();
        return null;
    }
    private void createNewTask() {
        try {
            insert theTask;
            FeedItem post = new FeedItem();
            post.ParentId = c.id;
            post.Body = 'created a task';
            post.type = 'LinkPost';
            post.LinkUrl = '/' + theTask.id;
            post.Title = theTask.Subject;
            insert post;
        } catch(System.Exception ex){
            system.debug('ERROR: ' + ex.getMessage());
            lastError = ex.getMessage();
        }
    }
}