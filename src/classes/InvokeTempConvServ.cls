public class InvokeTempConvServ {

		public Static String Converter(String InputCelcius)
		{
				
			TemperatureConverter.TempConvertSoap SInp = new TemperatureConverter.TempConvertSoap();
			
			TemperatureConverter.CelsiusToFahrenheitResponse_element resp = new TemperatureConverter.CelsiusToFahrenheitResponse_element();
			
			String FarehOut = resp.CelsiusToFahrenheitResult;
			
			FarehOut = SInp.CelsiusToFahrenheit(InputCelcius);
			
			return FarehOut; 
	}
}