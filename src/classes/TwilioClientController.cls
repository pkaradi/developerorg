public class TwilioClientController {
       
        private TwilioCapability capability;
        public String uname {get; set;}
        public String ustatus {get;set;}
        
        //****** Initalizer - Set up Twilio Capability Token - requires ApplicationSid__c to be set
        public TwilioClientController() {
            capability = TwilioAPI.createCapability();
            capability.allowClientOutgoing(
                TwilioAPI.getTwilioConfig().ApplicationSid__c,null);
                capability.allowClientIncoming('salesforce_agent');
                User currentAgent = [Select Id,Name,Status__c from User where Id=:userinfo.getuserId()];
                if(currentAgent.Status__c != null)
                ustatus = currentAgent.Status__c ;
                else
                ustatus = null;
        }
        
        //******* {!Token} method returns a string based capability token
        public String getToken() { return capability.generateToken(); }
        
        //******* Pass in a parameter PhoneNumber - and this will Dial the phone number
        public String getDial() {
            String callerid = TwilioAPI.getTwilioConfig().CallerId__c;        
            //phone number will be passed as http query parameter
            String PhoneNumber = System.currentPageReference().getParameters().get('PhoneNumber');
            TwilioTwiML.Response res = new TwilioTwiML.Response();
            
            TwilioTwiML.Dial d = new TwilioTwiML.Dial(PhoneNumber);
            d.setCallerId(callerid);
            res.append(d);
            return res.toXML();
        }
        // handle the action of the commandButton
    public PageReference updateAgentStatus() {
        System.debug('Status: '+ustatus);
        User currentAgent = [Select Id,Name,Status__c from User where Id=:userinfo.getuserId()];
        currentAgent.Status__c = ustatus;
        update currentAgent;
        return null;
    }
    /**
        * Function to get Location picklist value
        */
        public List<SelectOption> getStatus(){

            List<SelectOption> options = new List<SelectOption>();
            Set<String> picklistSet = new  Set<String> ();
            List<Schema.PicklistEntry> pick_list_values = User.Status__c .getDescribe().getPickListValues();
            options.add(new SelectOption('','Status'));
            if(pick_list_values != null && pick_list_values.size() >0 ){
                for (Schema.PicklistEntry a : pick_list_values) { 
                    /**
                    * for all values in the picklist list
                    */
                    if(a.getValue() != null){ 
                       picklistSet.add(a.getValue());
                    }
                 } 
             }
            
            /**
            * add all picklist values as a select option
            */
            if(picklistSet != null && picklistSet.size() > 0){
                for(String status: picklistSet){
                    if(status!= null){ 
                        options.add(new SelectOption(status,status));
                    }
                } 
            }
             
            
            return options; 
        } 
    }