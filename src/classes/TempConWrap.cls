public with sharing class TempConWrap {
	
	public String InputCelcius {get; set;}
	
	public String OutFareh {get; set;}
	
	public pagereference GetTemp()
	{
		OutFareh = InvokeTempConvServ.Converter(InputCelcius);
		return null;
	} 
}