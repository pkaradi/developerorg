global class AccountWebService 
{
    webservice static Account AccountWebService (String AccountName )
    {
        Account c = new Account();
        c = [SELECT Name, CustomerPriority__c, Active__c, AccountNumber FROM ACCOUNT WHERE NAME = :AccountName Limit 1];
        return c;
    }
}