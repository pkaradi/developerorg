<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send Approval Notification</fullName>
        <ccEmails>pkaradi@deloitte.com</ccEmails>
        <protected>false</protected>
        <recipients>
            <recipient>VP, Marketing</recipient>
            <type>role</type>
        </recipients>
        <template>unfiled$public/MarketingProductInquiryResponse</template>
    </alerts>
    <alerts>
        <fullName>Test</fullName>
        <protected>false</protected>
        <recipients>
            <recipient>pkaradihp@deloitte.com</recipient>
            <type>user</type>
        </recipients>
        <template>unfiled$public/Test</template>
    </alerts>
    <fieldUpdates>
        <fullName>Test FU</fullName>
        <field>StageName</field>
        <literalValue>Proposal/Price Quote</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Test</fullName>
        <actions>
            <name>Test</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Test FU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.DeliveryInstallationStatus__c</field>
            <operation>equals</operation>
            <value>Yet to begin</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
