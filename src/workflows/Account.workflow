<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Field Update</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Reject</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>updatePhoneURL</fullName>
        <field>PhoneUrl__c</field>
        <formula>IF( ISPICKVAL(DNC_Status__c , &apos;On DNC&apos;), 
&apos;&apos; , 
Phone  
)</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>updatePhoneLink</fullName>
        <actions>
            <name>updatePhoneURL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Phone</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
