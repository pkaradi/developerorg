<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Calculate Concur Value</fullName>
        <field>AmountConcur_Workflow__c</field>
        <formula>IF( ISPICKVAL( Currency__c , &quot;Local&quot;) , AmountLocal__c * Trip__r.Local2Concur__c, AmountLocal__c)</formula>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Not Reimbursable</fullName>
        <field>isReimbursable__c</field>
        <literalValue>0</literalValue>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Calculate Concur Value</fullName>
        <actions>
            <name>Calculate Concur Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Zero value receipt is Not Reimbursable</fullName>
        <actions>
            <name>Not Reimbursable</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Expense__c.AmountLocal__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
